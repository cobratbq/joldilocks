/**
 * Definition of the joldilocks module.
 */
module joldilocks {
    // FIXME `jsr305` is filename-based module declaration, should not be published to public repository.
    requires static jsr305;

    requires org.bouncycastle.provider;

    exports nl.dannyvanheumen.joldilocks;
}
