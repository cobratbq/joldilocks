# [Edwards curves](<https://www.hyperelliptic.org/EFD/g1p/auto-edwards.html> "Explicit Formulas Database: Edwards curves")

An elliptic curve in Edwards form [[database entry](<https://www.hyperelliptic.org/EFD/g1p/data/edwards/coordinates>); [Sage verification script](<https://www.hyperelliptic.org/EFD/g1p/auto-sage/edwards/coordinates.sage>); [Sage output](<https://www.hyperelliptic.org/EFD/g1p/auto-sage/edwards/coordinates.out>)] has parameters `c` `d` and coordinates `x` `y` satisfying the following equations:

```
  x2+y2=c2*(1+d*x2*y2)
```

Affine addition formulas: `(x1,y1)+(x2,y2)=(x3,y3)` where

```
  x3 = (x1*y2+y1*x2)/(c*(1+d*x1*x2*y1*y2))
  y3 = (y1*y2-x1*x2)/(c*(1-d*x1*x2*y1*y2))
```

Affine doubling formulas: `2(x1,y1)=(x3,y3)` where

```
  x3 = (x1*y1+y1*x1)/(c*(1+d*x1*x1*y1*y1))
  y3 = (y1*y1-x1*x1)/(c*(1-d*x1*x1*y1*y1))
```

Affine negation formulas: `-(x1,y1)=(-x1,y1)`.
This curve shape was introduced by 2007 Edwards for the case `d=1`.

Technically, an Edwards curve is not elliptic, because it has singularities; but resolving those singularities produces an elliptic curve.

The neutral element of the curve is the point `(0,c)`. The point `(0,-c)` has order `2`. The points `(c,0)` and `(-c,0)` have order `4`.
